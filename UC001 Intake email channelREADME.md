MetFlow: Use Case 001

Case No:	INT100
Date:	12/12/2016
Use Case/ Sub Part	Intake - E-mail Channel (au.services) for Intake Process

Document Responsibility
Subject Area	Owner	Date Completed	Status (Started, Revising, Under Review, Completed)
PURPOSE	CoE, BA	15/09	Completed
Actors / Stakeholders	CoE, BA, IT AU, Cogz	15/09	Completed
Triggers	CoE, BA, IT AU, Cogz	15/09	Completed
Pre Conditions	CoE, BA, IT AU, Cogz	17/09	Under Review
Post Conditions	CoE, BA, IT AU, Cogz	17/09	Completed
Integration	Cogz	05/10/2016	Completed
System Flow Diagram	CoE, BA	21/09	Completed
Activity Level Process	CoE, BA	21/09	Completed
Main Flow	CoE, BA	21/09	Completed
Alternative Flow	N/A		Completed
Sample UI	N/A		Completed
Database	To be finalized with calibration during build	
Know Issues/Dependencies	CoE, BA	27/09	Completed
Test Cases & Criteria	Cogz	21/10	Completed
Table of Contents
1	Purpose	4
1.1	Document Conventions and Use Case Elements	4
1.1.1	Use Case Coverage (Purpose)	4
1.1.2	Primary Participant/Role (Actors/Stakeholders)	4
1.1.3	Triggers	5
1.2	Assumptions & Dependencies - C	5
2	Use Case INT100 – Intake Claims, Electronic Only (Emails only)	7
2.1	Purpose - C	7
2.2	Actors and Stakeholders - C	7
2.3	Triggers – C*	7
2.4	Precondition - C	7
2.5	Post Condition - C	8
2.6	Integration Points - C	8
2.7	System Flow Diagram - C	9
2.8	Activity Level Process - C	10
2.9	Main Flow - C	11
2.10	Alternative Flow - C	11
2.11	Sample UI - C	11
2.12	Database	11
2.13	Know Issues / Dependencies - C	12
2.14	Test Criteria - C	12

 
1	Purpose
The Claims team would like MetFlow to replace the existing processes in PICS and create an improved Claims process flow using the BizFlow system. The process work flow system will integrate with internal applications of GLSCI, ALIS, eAPPLY, MetBI, B2B and eLodgement as well as some external parties UHG.
This Business Requirements Specifications (BRS) document captures the business functions and processes within the scope of MetFlow Australia Claims process for Release 1.  
It documents the desired business process architecture, describing in detail how the target solution will address identified pain points through process improvement, automation, and integration with existing systems.  The BRS is one of two documents that will be used as road maps for the transformation and automation of the individual and group Claims process 
The BRS and BDS together present the results of the Analysis and Design phase for the MetLife Australia Operations “MetFlow” project. .  The project involves: Business process design and improvement;  
There will be selective integration of the BPM-based solution with existing systems such as e-Lodgment, e-Claims, Iron MT etc. In the Business Case for the MetFlow project, the term “light integration” or “selective integration” is defined as “Focus on a limited number of integration quick wins with significant impacts e.g. to resolve duplicate data entry introduced by the BPM deployment, or to automate some activities.” For this initial release of the MetFlow solutions, scheduled for mid- 2017, the BPM system will integrate with the following systems as described below:

•	e-Claims:
•	e-Lodgment:
•	AirDocs
•	GL/SCI
•	MetBi
•	Iron Mountain
•	Existing Claim and Policy system

1.1	Document Conventions and Use Case Elements
The MetFlow Claims functional requirements are captured in a series of Use Cases.  This section describes some of the elements within the Use Cases   
1.1.1	Use Case Coverage (Purpose)
Provides an overview of the scope of the Use Case allowing for identification of scenarios that have been captured and those that have yet to be captured in MetFlow.	
1.1.2	Primary Participant/Role (Actors/Stakeholders)
This lists the participant (actor) who has the primary interest in the outcome of this Use Case or whose goal will be satisfied by the Use Case. The primary participant is often, but not always, the participant who triggers the Use Case. The participant (actor) may be a person or a software component.
Secondary Participants:  Lists the participant(s) who have a supporting role in helping the Primary Participant to achieve his or her goal.  Secondary participants help the BPM system deliver one or more services to the Primary Participant. Examples include Web Services, other humans, an external system or database.
1.1.3	Triggers
Triggers, these are event(s) that start(s) this Use Case.
Assumptions: Lists those facts that are assumed to be true, but may later prove to be untrue.  Assumptions differ from pre-conditions in that the BPM system does not verify the assumption prior to the start of the Use Case. If the assumption is proven false during the execution of the Use Case, the system handles this outcome with one of the Alternate (or Exception) Paths.
Flow of Events – Basic Path: Describes the main flow of events. That is, the steps narrating/illustrating the interaction between participants and the system. This is the ‘happy path’ scenario, meaning the straight and simple path where everything goes ‘right’ and enables the primary participant to accomplish his or her goal. 
Flow of Events – Alternative Path: Describes branches from the main flow of events to handle special conditions.
Business Requirements: describes in greater detail what the BPM system needs to do to complete the use case.
Data Elements: Describes data that is entered by the user, auto-populated by the BPM system, updated by the user or system, or displayed by the system.

1.2	Assumptions & Dependencies - C
Assumptions
1.	The BPM System will be accessible to internal MetLife users only.  Other parties such as brokers, agents, policy holders, members, and service providers will not have access to the solution.  
2.	The BPM System will be accessible remotely, via VPN connection to the MetLife network subject to MetLife’s access and authorization procedures and protocols
3.	MetLife IT security standards will be applied to the solution to protect and preserve the confidentiality and integrity of policy and claims data while promoting the availability of such data for authorized use
4.	The dependent systems e-lodgement, e-claims, AirDocs, GL/SCI services are available 24x7 in Australia” E-Lodgement and e-claims system are front office system and will communicate asynchronously to BizFlow using MQ via IIB. GL/SCI and Airdocs are 24x7 but availability of these services is 99.96SLA. BMP should be able to handle the exception cases”
5.	BPM system throughput will have consideration based upon data available within BPM database and not  on the other systems Performance  (like B1MCO)
6.	BPM Support model lies with Australia teams
7.	BPM System Roles and Responsibilities for Australia are solely for BPM application Process Flow Execution purpose and does not co-relate with other systems.
8.	BPM system will have standard login page Australia application

Dependencies
1.	External System 
-	UHG
-	AirDocs
-	Iron Mountain

2.	Internal Systems:
-	e-Claims:
-	e-Lodgement:
-	GL/SCI
-	MetBi
-	Existing Claim and Policy system

 
2	Use Case INT100 – Intake Claims, Electronic Only (Emails only)
MetLife has a company mail box defined as au.services, where correspondence for various Operational teams is received.

2.1	Purpose - C
The purpose of the use case is to address Claims’ Intake process via email from the au.services mail box into the Claims Mail Box.
•	BPM schedule processor will read the email subject line from all designated email id that are allocated to the Claims Mail Box, which have been manually transferred by the Customer Experience Consultants from the au.services mails box to the Claim Mail Box. If the case ID exist on MetFlow the work item should be automatically allocated to the Claim Assessor
•	BPM will create a unique claim id by using GL/SCI service once a Claim Manager has identified the type of Claim and allocated a non-indexed case to a Claim assessor during the triage process. The Case ID will be created after the Triaging, this way the BPM system is aware of the type of claim, and which operational team it belongs to.

2.2	Actors and Stakeholders - C
Below table depicts Actors details
Actor	Role	Comments
System(BPM)	Primary Participant	MetFlow reads Email Subject
Claim Manager	Triage New Claim	GLSCI creates New Claim Number

2.3	Triggers – C*

•	E-mails arrive in au.services Mail Box, every  e-mail is triaged and then allocated to a New Team Mail Boxfolder manually:
- Claims Mail Box
- Customer Relations (CDF) Mail Box
- Customer Experience (Admin) Mail Box
- Rehabilitation Mail Box
•	The BPM schedule processor will monitor the Claim Inbox (email) every hour from 8.30 am to 5.30 pm and initiate the reading of the header, if Case ID is valid allocate to Claim Assessor.
•	All other Mail Boxes will have same process
•	However if the Cased ID has no valid Case Reference in the Claims Folder, then MetFlow will create a Work Item for the CE team to triage, where they can then identify the Claimant Name and the Claim Type, so that the correct Claims Team assess the correct claim. (the process of triage is just like the Iron Mountain triage process)
•	For all other Operational Teams, MetFlow will take the work items and allocate to the Manager of the team to allocate to an assessor or consultant.
•	 WDL Logic is required for creation of Work Item in each Mail Box Folder
The above has been re defined BF 12/12/2016 – MR please edit your comments.

2.4	Precondition - C
•	BPM System scheduler is pre-configured and monitored the Email Inbox every 30 minutes 
•	The scheduler will run on a specific time interval on the various Mail Boxes, this release its just Claims Mail Box.
•	To initiate the MetFlow intake process to create a Work Item or to allocate to an existing case ID, the email should arrive into the monitored Claim inbox. (other Mail boxes when we draft other Use Cases)
•	There should be specific Case ID in heading of e-mail that needs to be identified by MetFlow for the BPM Processor to process the email as a work item for a Claim Assessor or Claim Administrator.
•	The Case ID is the Claim ID number created by GL SCI and will have a prefix unique based on the Claim Type, that will be created by MetFlow
2.5	Post Condition - C
•	BPM system process email and sends it for triaging to either Claim’s administrator queue or   Customer experience team queue depends on Subject reference of the emails.
•	If no email is received, there will not be any triaging process.
•	E-mails - SFTP
2.6	Integration Points - C
•	E-Lodgment and e-claims system are front office system and will communicate asynchronously to BizFlow using MQ via IIB. GL/SCI and Airdocs are 24X7 but availability of these services are 99.96SLA. BMP should be able to handle the exception cases”
•	Please refer to Integration documents produced by MetLife IT and Cognizant for the Claims Build
Integration Doc - AirDocs.docx
Integration Doc - GL SCI.docx
Integration Doc - Iron Mountain.docx
Integration Doc - MetFlow FO.docx

2.6.1	MetFlow Steps – C
•	Will need from Infrastructure Team set up of new mail boxes for Claims, CDF, Rehab and Customer Experience
•	Also au.services will have attachments (forms, claim documents)  that will need to be stored in FileNet Adaptor 
•	MetFlow should read and monitor Mail Box to identify cases with MetFlow Case File reference.
•	Definition of Case File naming please see Use Case

2.6.2	GL SCI Steps - C
•	No requirement 


 
2.7	System Flow Diagram - C

 
 

 

 
 

  
2.8	Activity Level Process - C
•	Email will arrive on the monitored inbox - au.services@metlife.com
•	Customer Experience team identify Claim cases and allocates the e-mails into the Claims Mail Box
•	BPM system reads and validates the Email Subject line by identifying the Case ID 
•	Based upon the Case ID, the BPM system will assign the intake case to a Claim assessor
•	If the Intake process conditions gets validated against the Claim Administrator where the Subject Reference condition meets the criteria, the Claim Admin will triage the Claim
•	If the Intake process conditions gets validated against the Customer Experience team, where the Subject Reference condition doesn't meet the  criteria for Claim Administrator, the CE team will triage the Claim
•	Triaging of a claim will be performed by the Claim Managers and  their deputies
•	After Triaging, based upon the triaging condition, the claim will route to the claim process. 
•	The intake channels sends the information to the BPM system
•	The intake channels comprises of the following:
AU Services
-	Sends the following details
 	- BPM Claim reference id (optional)
- Documents as an attachments
•	The BPM system check the intake channels type whether it is from Iron Mt or any other listed above channels and determine whether triaging is required or not.
•	If triaging is required, the BPM case will be forwarded to the claim team for triaging process, once the triaging is performed, the BPM case will be undergoing for claim case processing finally.
•	If triaging is not required, the BPM case will be undergoing for claim case processing finally.
•	The activity finishes

2.9	Main Flow - C
•	BPM ERA Process reads the incoming email from AU Mailbox (au.services@metlife.com) location and initiates the intake process. 
•	Channel Type is - Email
•	BPM System process verifies email subject line (to be determined) and will send the necessary attachments, details to either Claim Admin or CE team for triaging.
-  Based upon the triaging condition, the claim will initiate the claim process

2.10	Alternative Flow - C
 Not Applicable

 
2.11	Sample UI - C
Not required for this Use Case
There is no UI – but using the current new Business front end screen, we have designed an example:
U/W Exampl
 
Customer Experience ‘My Work ‘ Screen
 


2.12	Database
This will be finalised during the Build and Design collaboration workshops

Table Name	Columns	Comments
		
		

 
2.13	Know Issues / Dependencies - C
•	Email Subject Line is not yet formalized.
•	AU Shared email box and email contents needs to be confirmed
•	Conditional logic for Subject line needs to be confirmed. 
•	For Release 1 scope, triaging will only happen for Claim and not for CDF, Admin type of cases.

2.14	Test Criteria - C

•	As a CE user, 
-	I’ll able to login to MetFlow Claim system
-	I can navigate to the au.services@metlife.com inbox(as is)
-	I can see all received emails available in au.services@metlife.com inbox and where the BPM claim reference id (optional) is provided with any document attachments (optional).
-	I can identify various Operational work request and other request, including Claim cases and then the CE team can drop them into the relevant ‘Operational Inbox’ (each team will have its own In Box).

•	The BPM System will access Work Item case and automatically assigns these cases to Claim assessor provided the Case ID is in the heading. Where the case ID is not in the heading, the work item will be allocated to the Claim Manager for Triaging.
(Work Item will have the Case id and match with the email received).

•	The BPM system will assign the case to Claim Manager/Deputies where there is No Case Id or the Case ID has not been registered in MetFlow or is in valid. 

•	Claim manager verify the case based on the Claim Type and BPM system should allow allocating the claims to claim administrator or CE user based on the subject reference condition criteria (what is the criteria?)

•	BPM System will allow claim administrator or Claim Manager to triage the claim case based on Claim Type and their ability to determine which Claim Assessor can assess this case.



 
2.15	Outstanding Tasks **
•	Definition of Case ID required
•	Definition of Mail Boxes to be done by MetLife IT
•	Process for manual Intake though mail Inbox
•	Process for manual triage of cases in mailbox by Managers / Deputies


